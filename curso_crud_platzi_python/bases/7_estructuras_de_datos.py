lista = [1,2,3]
print('lista', lista)

lista[1] = 4
print('lista_actualizada', lista)

otra_lista = [3,2,1]
print('listas_concatenadas', lista + otra_lista)

listica = [1]
print('multiplicada', listica * 5)

lista.append(4)
item_borrado = lista.pop(0)
lista.sort()

print('lista nueva', lista)

lista_con_range = list(range(0, 100, 2))

name = 'marlon'
print('my name is {}'.format(name))



# ******************************

diccionario = {'name': 'marlon', 'age': 31}
print('diccionario', diccionario)
print('el que no existe', diccionario.get('apellido', 'no hay apellido'))

for key in diccionario.keys():
    print(key)

for value in diccionario.values():
    print(value)

for key, value in diccionario.items():
    print(key, value)

# ******************************

# tuplas son inmutables, no funciona el tuple[2] = some_value
tuplas = (1,2,3)

# sets no pueden tener elementos repetidos
# los sets contienen metodos relacionados a teoria de conjuntos
# intersection, union, difference, etc
some_set = set([1,2,2,3])
some_set.add(2)
print('some_set', some_set)