for i in range(5):
    print(i)

names = 'marlon,caro,laura,dora'
namesList = names.split(',')

for name in namesList:
    print(name)

n = 0
while n < 10:
    print('while', n)
    n += 1