type_variable = type('marlon')
print('type_variable', type_variable)

convertir_string_to_int = int('5')
print('convertir_string_to_int', type(convertir_string_to_int))

convertir_a_float = float(3)
print('convertir_a_float', type(convertir_a_float))

def suma_de_dos_numeros(x, y):
    return x + y

print('suma_de_dos_numeros', suma_de_dos_numeros(2, 3))