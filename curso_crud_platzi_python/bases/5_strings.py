name = 'marlon'

segunda_letra = name[1]
print('segunda_letra', segunda_letra)

penultima_letra = name[-2]
print('penultima_letra', penultima_letra)

length = len(name)
print('length', length)

print(name.upper())
print(name.lower())
print(name.capitalize())

print(name.startswith('m'))
print(name.endswith('n'))
print(name.find('r'))

# ayuda
# print(dir(name))

def sumar(x, y):
    return x + y

# print(help(sumar))

# slices

print('mitad', name[2:4])
print('inicio', name[0:2])
print('inicio', name[:2])
print('final', name[4:])

# el tercer parametro le dice que se salte caracteres
print('test', name[1:-1:2])

