suma = 2 + 2
print('suma', suma)

resta = 3 - 1
print('resta', resta)

division_sin_decimales = 10 // 3
print('division_sin_decimales', division_sin_decimales)

modulo_de_division = 10 % 3
print('modulo_de_division', modulo_de_division)

multiplicacion = 3 * 3
print('multiplicacion', multiplicacion)

exponente = 2 ** 6
print('exponente', exponente)

type_number = type(5)
print('type_number', type_number)

concatener_strings = 'marlon' + ' ' + 'gomez'
print('concatener_strings:', concatener_strings)

repetir_strings = '_lindo_' * 5
print('repetir_strings', repetir_strings)