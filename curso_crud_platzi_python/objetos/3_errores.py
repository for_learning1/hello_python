def dividir(numerador, denominador):
    if denominador == 0:
        raise 'denominador no puede ser cero'
    return numerador / denominador

print(dividir(2,2))

# try:
#     print(dividir(2,0))
# except expression as identifier:
#     print('errorcito')

PI = 3.14159
print(PI)
PI = 'MARLON'
print(PI)

print(5//2)

name = 'james'
if not name.startswith('j'):
    print('Hello')
else:
    print('world')