Django==3.0.8
openpyxl==3.0.4
pandas==1.0.5
Pillow==7.2.0
pylint==2.5.3
pylint-django==2.0.15
psycopg2==2.8.5
xlsxwriter==1.2.9