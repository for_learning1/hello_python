import json
from datetime import datetime
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User as AdminUser
from django.shortcuts import redirect, render
from posts.models import User
from posts.forms import PostForm

posts = [
    {
        'name': 'Mont Blac',
        'user': 'Marlon Gomez',
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
    }
]

# Create your views here.

@login_required
def list_posts(request):

    content = []
    for post in posts:
        content.append("""
            <p><strong>{name}</strong></p>
            <p><span>{user}</span></p>
            <p><small>{timestamp}</small></p>
        """.format(**post))

    return HttpResponse(content)

@login_required
def list_posts_2(request):
    return render(request, 'posts/feed.html', {'posts': posts})

def create_user(request):
    marlon = User.objects.create(
        email='marlonsgy4@gmail.com',
        password='1234',
        first_name='marlon',
        last_name='gomez'
    )

    caro = User()
    caro.email = 'caro2@caro.com'
    caro.password = '1234567'
    caro.first_name = 'caro'
    caro.last_name = 'herrera'
    caro.save()

    data = {
        'marlon': {
            'user_id': marlon.pk,
            'email': marlon.email,
            'last_name': marlon.last_name
        },
        'caro': {
            'user_id': caro.pk,
            'email': caro.email,
            'last_name': caro.last_name
        }
    }

    caro.delete()

    return HttpResponse(json.dumps(data, indent=4), content_type='application/json')

def get_user(request):
    marlon = User.objects.get(email='marlonsgy@gmail.com')
    data = {
        'marlon': {
            'user_id': marlon.pk,
            'email': marlon.email,
            'last_name': marlon.last_name
        }
    }
    return HttpResponse(json.dumps(data, indent=4), content_type='application/json')


def get_all_users(request):
    users = User.objects.filter(email__endswith='@caro.com')

    data = {
        'caro': {
            'user_id': users[0].pk,
            'email': users[0].email,
            'last_name': users[0].last_name
        }
    }
    return HttpResponse(json.dumps(data, indent=4), content_type='application/json')

def create_admin(request):
    new_admin = AdminUser.objects.create_user(username='marlon', password='1234')
    data = {
        'marlon': {
            'user_id': new_admin.pk,
            'email': new_admin.username,
            'last_name': new_admin.password
        }
    }
    return HttpResponse(json.dumps(data, indent=4), content_type='application/json')

def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('feed')
    else:
        form = PostForm()

    return render(request=request, template_name='posts/new.html', context={
        'form': form
    })
