from django.urls import path

from posts import views

urlpatterns = [
    # path('posts/', views.list_posts, name='listica'),
    path(
        route='posts/',
        view=views.list_posts,
        name='listica'
    ),

    path('posts2/', views.list_posts_2, name='feed'),
    path('posts_users/', views.create_user),
    path('posts_get_user/', views.get_user),
    path('posts_get_all/', views.get_all_users),
    path('adminar/', views.create_admin),
    path('posts/new/', views.create_post, name='create_post'),
]