from django.http import HttpResponse
from datetime import datetime
import json

def hello_world(request):
    now = datetime.now().strftime('%b %dth, %Y - %H:%M hrs')
    return HttpResponse('Hello, Marlon G. Current time is {}'.format(now))

def sorted(request):
    # import pdb
    # pdb.set_trace()
    # return HttpResponse(request.GET['numbers'].split(','))
    # return "request.GET['numbers']"
    numbers = [int(i) for i in request.GET['numbers'].split(',')]
    sorted_numbers = sorted(numbers)
    data = {
        'status': 'ok',
        'numbers': sorted_numbers,
        'message': 'Integers sorted succesfully'
    }
    return HttpResponse(json.dumps(data, indent=4), content_type='application/json')

def say_hi(request, name, age):
    if age < 12:
        message = 'sorry, you are not allowed'
    else:
        message = 'welcome, {}'.format(name)
    return HttpResponse(message)