from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse

class PlatzigramMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not request.user.is_anonymous:
            # return HttpResponse(request.user.username)
            # user = request.user
            if request.path != reverse('listica'):
                return redirect('listica')
            # if user.username == 'marlonsgy':

        return self.get_response(request)
