"""platzigram URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include

from platzigram import views as local_views
# from posts import views as posts_views
# from users import views as users_views
from exporters import views as exporter_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello-world/', local_views.hello_world),
    path('sorted/', local_views.sorted),
    path('hi/<str:name>/<int:age>/', local_views.say_hi),

    path('', include(('posts.urls', 'posts'), namespace='posts')),
    path('', include(('users.urls', 'users'), namespace='users')),

    path('query', exporter_views.get_data, name='query'),
    # path('clients', exporter_views.get_clients, name='clients')

    # path('posts/', posts_views.list_posts, name='listica'),
    # path('posts2/', posts_views.list_posts_2, name='feed'),
    # path('posts_users/', posts_views.create_user),
    # path('posts_get_user/', posts_views.get_user),
    # path('posts_get_all/', posts_views.get_all_users),
    # path('adminar/', posts_views.create_admin),
    # path('posts/new/', posts_views.create_post, name='create_post'),

    # path('users/signup/', users_views.signup, name='signup'),
    # path('users/login/', users_views.login_view, name='login'),
    # path('users/logout/', users_views.logout_view, name='logout'),
    # path('users/me/profile/', users_views.update_profile, name='update_profile'),
    # path(
    #     'users/me/profile_form/',
    #     users_views.update_profile_with_form,
    #     name='update_profile_with_form'
    # ),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
