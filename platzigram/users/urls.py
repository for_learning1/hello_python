from django.urls import path
# from django.views.generic import TemplateView

from users import views

urlpatterns = [

    path(
        route='<str:username>/',
        view=views.UserDetailView.as_view()
        # view=TemplateView.as_view(template_name='users/detail.html')
    ),

    path('users/signup/', views.signup, name='signup'),
    path('users/login/', views.login_view, name='login'),
    path('users/logout/', views.logout_view, name='logout'),
    path('users/me/profile/', views.update_profile, name='update_profile'),
    path(
        'users/me/profile_form/',
        views.update_profile_with_form,
        name='update_profile_with_form'
    ),
]