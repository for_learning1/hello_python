import json
import os
from datetime import datetime

from django.db import connection
from django.http import HttpResponse

import pandas as pd

def get_data(request):
    start = get_now()

    query_str = query()
    data = fetch_from_query(query_str)
    export_to_excel(data, get_absolute_path('data.xlsx'))

    end = get_now()
    return get_nice_response({
        'start': format_date(start),
        'end': format_date(end),
        'diff_in_secs': diff_in_secs(start, end),
    })

def diff_in_secs(start, end):
    return (end-start).total_seconds()

def get_now():
    return datetime.now()

def format_date(date):
    return date.strftime('%Y-%m-%d-%H:%M:%S')

def get_nice_response(data):
    return HttpResponse(json.dumps(data, indent=4), content_type='application/json')

def query():
    file_path = get_absolute_path('query.txt')
    data_file = open(file_path, 'r')
    return data_file.read()

def get_absolute_path(filename):
    module_dir = os.path.dirname(__file__)
    return os.path.join(module_dir, filename)

def delete_file(filename):
    os.remove(filename)

def fetch_from_query(query_str):
    with connection.cursor() as cursor:
        cursor.execute(query_str)
        data = cursor.fetchall()
        columns = [col[0] for col in cursor.description]
        data.insert(0, columns)
    return data

def export_to_excel(data, filename):
    delete_file(filename)

    sheetname = 'sheet_export'
    data_frame = pd.DataFrame.from_dict(data)

    writer = pd.ExcelWriter(filename, engine='xlsxwriter')
    data_frame.to_excel(
        writer,
        sheet_name=sheetname,
        index=False,
        header=False
    )

    worksheet = writer.sheets[sheetname]

    for idx, col in enumerate(data_frame):
        series = data_frame[col]
        max_len = max((
            series.astype(str).map(str_len).max(),
            str_len(str(series.name))
            )) + 2
        worksheet.set_column(idx, idx, max_len)
    writer.save()

def str_len(some_string):
    try:
        row_l = len(some_string)
        utf8_l = len(some_string.encode('utf-8'))
        return (utf8_l - row_l) / 2 + row_l
    except:
        return None
    return None
