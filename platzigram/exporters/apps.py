from django.apps import AppConfig


class ExportersConfig(AppConfig):
    name = 'exporters'
